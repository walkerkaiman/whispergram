class Pixel{
  PVector pos;
  color fill;
  int size;
  
  Pixel(PVector _pos, int _size, color _fill){
    pos = _pos;
    size = _size;
    fill = _fill;
  }
  
  void display(){
    noStroke();
    fill(fill);
    
    rect(pos.x, pos.y, size, size); 
  }
}