import hypermedia.net.*;

UDP udp;

ArrayList <Pixel> all_the_pixels;
int y = 0;
int pixelSize = 10;

void setup() {
  udp = new UDP( this, 8888 );
  udp.log( true ); 
  udp.listen( true );
  
  all_the_pixels = new ArrayList<Pixel>();
  
  size(360, 700);
}

void draw() {
  background(0);
  
  for(int i = 0; i < all_the_pixels.size(); i++){
    Pixel p = all_the_pixels.get(i);
    p.display();
  }
}

void receive( byte[] data ) { 
  String message = new String( data );
  println(message);
  
  int x = all_the_pixels.size()*pixelSize % width;
    
  if(x == 0){
    y += pixelSize; 
  }
  
  if(message.equals("1")){
    all_the_pixels.add(new Pixel(new PVector(x, y), pixelSize, color(255))); 
  }
  else{
    all_the_pixels.add(new Pixel(new PVector(x, y), pixelSize, color(0)));
  }
}